import { Component, OnInit } from '@angular/core';
import {RestService} from '../rest.service';

@Component({
  selector: 'app-oauth-page',
  templateUrl: './oauth-page.component.html',
  styleUrls: ['./oauth-page.component.css']
})
export class OauthPageComponent implements OnInit {

  constructor(private rs: RestService) { }

  ngOnInit() {
  }

  askForAuthentication() {
    this.rs.askForAuth().subscribe( response => console.log(response));
  }
}
