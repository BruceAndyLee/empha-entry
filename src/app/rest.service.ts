import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  vkHost = 'https://oauth.vk.com/authorize';
  private host = 'http://82.146.41.50/';
  private clientId = 7474217;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
  };

  constructor(private http: HttpClient) { }

  askForAuth() {
    return this.http.get(
      this.vkHost +
      '?client_id=' + this.clientId +
      '&response_type=token' +
      '&redirect_uri=https://oauth.vk.com/blank.html' +
      '&v=5.59', this.httpOptions);
  }
}
